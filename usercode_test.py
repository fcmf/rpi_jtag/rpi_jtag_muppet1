#!/usr/bin/env python3
from time import sleep
from jtag import *
import struct
from datetime import datetime

# Read IDCODE using hardware SPI interface
TLR_RTI()
#prep_shift_dr() # Prepare shifting in/out data
TMS1()
TMS0()
TMS0()
disable_gpios()
r = spi_read(4)
binstr = ""
for byte in r:
    binstr += "{:08b}".format(byte)
binstr = binstr[::-1]
print("IDCODE read from device using SPI interface:\n0x{:08x}".format(int(binstr,2)))

# Read USERCODE register
enable_gpios()
TLR_RTI()
prep_shift_ir()
print("Loading instruction USERCODE")
load_instr("USERCODE")
TMS1()
TMS0()
prep_shift_dr()


disable_gpios() # Prepare using the hardware SPI interface

r = spi_read(4)

'''
timeint = int.from_bytes(r,signed=False,byteorder='little')
print("Integer from bytes: {}".format(timeint))
print("Int as HEX: {:08X}".format(timeint))
readable = datetime.fromtimestamp(timeint).isoformat()
print("Decoded time from IDCODE timestamp: {}".format(readable))
'''

binstr = ""
for byte in r:
    binstr += "{:08b}".format(byte)
binstr = binstr[::-1]
timeint = int(binstr,2)
timehex = "0x{:08x}".format(timeint)
print("USERCODE read from device using SPI interface:\n{}".format(timehex))

readable = datetime.fromtimestamp(timeint).isoformat()
print("Decoded time from IDCODE timestamp: {}".format(readable))

enable_gpios()




