# RaspberryPi JTAG interface for module MUPPET1

The MUPPET1 module has a 40-pin header at the back of the board tailored to
connect a RaspberryPi module (RPI) with it's 40-pin HAT connector. Doing so connects
multiple GPIOs as well as two SPI interfaces of the RPI to the FPGA on the
MUPPET1 module. One of the SPI interfaces goes to the JTAG/TAP port of the
FPGA. The programs in this respository provide a commandline tool to program a
user bitstream directly into the FPGA SRAM or to write it to the attached SPI Flash
configuration memory for permanent storage and automatic re-configuration of
the FPGA at power-up.

## How to:

Connect a RPI module upside-down to the 40-pin header at the back edge
of the MUPPET1 module so that the RPI module sticks out at the back/
does not overlap with the MUPPET1 module (only with the connector).

The commandline tool is called program_muppet1.py and has the following usage:

```
user@rpi:~/rpi_jtag_muppet1$ ./program_muppet1.py -h
usage: program_muppet1.py [-h] [-lf] [-nv] [-vo] bitfile

positional arguments:
  bitfile            specify path of bitfile to be loaded

optional arguments:
  -h, --help         show this help message and exit
  -lf, --loadFlash   load bitstream into non-volatile configuration flash
                     memory
  -nv, --noVerify    only load bitstream into flash memory, skip verification
  -vo, --verifyOnly  only verify flash content with specified bitfile
```

If the program is called with the path to a user bit stream and no other
options, the bitstream gets written directly to the FPGA SRAM (the FPGA gets
configured with this bitstream directly). If the -lf/--loadFlash option is
added, the bitstream gets written to the attached non-volatile SPI Flash
configuration memory, but not to the SRAM! -> Do not forget to restart the
FPGA to get the new bitstream active after a -lf/--loadFlash operation.

Note: The --noVerify and --verifyOnly options are not properly implemented, yet.
